<a href="https://gitlab.styleci.io/repos/8880111"><img src="https://gitlab.styleci.io/repos/8880111/shield?branch=1.0" alt="StyleCI"></a>

<h1>Documentation</h1> <br>

<h2>What Does This Package Do?</h2>
<span>This package basically provides "Currency CRUD" and "A Helper Which is Helps You to Choose Your Currency In FrontSide And Stores As Session" so you dont need to worry about managing your currencies</span>


<h2>Installation</h2>

<h4>Step 1</h4>
<code>composer require azizyus/currencymanager</code>
<br>

<h4>Step 2</h4>
Add 
<pre>
CurrencyManager\CurrencyManagerServiceProvider::class,
Former\FormerServiceProvider::class,
</pre>
to config/app.php

<h2>Step 3 (Migrations)</h2>
<span>
<code>php artisan vendor:publish --tag=azizyus/currencymanager-publish-migrations</code>
</span>
<br>
<span>Im already loading migrations with ->loadMigrationsFrom, but if it doenst work just publish them and use it <br> 
(that means you can migrate without publishing but in case if you want to override or etc... just do it as i said)</span>





<h2>Config File</h2>
<span>
<code>php artisan vendor:publish --tag=azizyus/currencymanager-publish-config</code>
</span>
<span>
<br>
<br>
<br>
<pre>

return [
    'usePackageRoutes'         => true,
    'middleWares'              => ['web'],
    'prefix'                   => 'currency',
    'frontPrefix'              => 'currency_front',
    'frontMiddlewares'         => ['web'],
    'controllerNamespace'      => 'CurrencyManager\Controllers',
    'frontControllerNamespace' => 'CurrencyManager\Controllers',
];

</pre>
</span>

<span>
As i mentioned at first, this package helps you too choose your 
currency in frontside so you have some config parameters about them,
you can change both of middlewares and prefixes or namespaces
</span>



<h2>Routes</h2>
<span>
I'm using Route::resource routes for crud operations for default and they are;
<br> currency.index 
<br> currency.edit
<br> currency.update
<br> currency.create
<br> currency.store
<br> currency.delete
<br>
and because of these also you need to pass {{method_fields(' ')}} if you want 
to customize package also there is front routes to change your currency;

<br> route("currency.change",["id"=>"_YOUR_CURRENCY_ID"]);
<br> route("currency.current"); 
<br>
last one shows your current currency as json for more like testing purposes i dont think anyone want to use that
</span>



<h2>Controllers</h2>
<span>
You will be have 2 controller in package;
CurrencyController, which is gives crud operations and CurrencyFrontController and this one gives you currency changing feature,
you can inheritance them and change some specific parts of like a edit page's view file or you can pass a modified repository with some of your specific operations
</span>


<h1>FAQ</h1>
<h2>What does CurrencyHelper do?</h2>
<span>
It manages your selected currency via sessions for you, all you need is; take instance and ->getCurrencyOrDefault(); and if you dont have any error ($currencyHelperInstance->errorThrower->hasError)
you can use your currency safely
</span>


<h2>I want to exchange my currency with another one what to do?</h2>
<span>
I got CurrencyExchangeCalculator for that, pass your base and target currency Instances to ctor and ->setPrice();
then you can calculate right price by ->baseToTarget();
</span>

<h2>I need to use my own controllers what to do?</h2>
<span>Just extend one of my controllers and currencyManager config file should look like this;</span>
<pre>
return [
    'usePackageRoutes'         => true,
    'middleWares'              => ['web'],
    'prefix'                   => 'currency',
    'frontPrefix'              => 'currency_front',
    'frontMiddlewares'         => ['web'],
    'controllerNamespace'      => "App\\Http\\Controllers",
    'frontControllerNamespace' => "App\\Http\\Controllers",
];
</pre>


<h2> i already inherited my controller what to do? </h2>
CurrencyController updated in <a href="https://gitlab.com/azizyus/currencymanager/commit/b1678f7ca67dfc52b0b28e9d6ad4f11e9cf9671b">b1678f7c</a> so you get these methods as 
trait but still you need to change config file 