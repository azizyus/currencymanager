<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.10.2018
 * Time: 05:01.
 */

namespace CurrencyManager\Error;

class YouHaveNoCurrencyInYourDatabase extends AbstractError
{
    public $errorMessage = 'Your Have No Currency In Your Database, at least you need 1 currency';
}
