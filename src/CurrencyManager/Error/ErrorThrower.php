<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.10.2018
 * Time: 05:03.
 */

namespace CurrencyManager\Error;

class ErrorThrower
{
    public $hasError = false;
    public $errors = [];

    public function setError(AbstractError $abstractError)
    {
        $this->errors[] = $abstractError;
        $this->hasError = true;

        if (env('APP_DEBUG') == 'true' && env('APP_ENV') == 'test') {
            throw new \Exception($abstractError->errorMessage);
        }
    }

    public function getErrors() : array
    {
        return $this->errors;
    }
}
