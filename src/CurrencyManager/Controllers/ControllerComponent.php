<?php


namespace CurrencyManager\Controllers;


use CurrencyManager\Factories\CurrencyCrudFormFactory;
use CurrencyManager\Factories\CurrencyHelperFactory;
use CurrencyManager\Factories\CurrencyRepositoryFactory;
use CurrencyManager\Forms\CurrencyCrudForm;
use CurrencyManager\Forms\StoreParams;
use CurrencyManager\Forms\UpdateParams;
use CurrencyManager\Repositories\CurrencyRepository;
use Illuminate\Http\Request;

class ControllerComponent
{


    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * @var CurrencyCrudForm
     */
    private $currencyCrudForm;
    private $currencyCrudFormQuickBuilder;

    /**
     * @var \CurrencyManager\Helpers\CurrencyHelper
     */
    private $currencyHelper;


    public function __construct()
    {
        $this->currencyRepository = $this->newInstanceCurrencyRepository();
        $this->currencyCrudForm = $this->newInstanceCurrencyCrudForm();
        $this->currencyHelper = $this->newCurrencyHelperInstance();
    }

    protected function newInstanceCurrencyRepository() : CurrencyRepository
    {
        $currencyRepository = CurrencyRepositoryFactory::makeForCrud();

        return $currencyRepository;
    }

    protected function newInstanceCurrencyCrudForm() : CurrencyCrudForm
    {
        return new CurrencyCrudForm();
    }

    protected function newInstanceCurrencyCrudFormFactory() : CurrencyCrudFormFactory
    {
        return new CurrencyCrudFormFactory();
    }

    protected function newCurrencyHelperInstance()
    {
        return CurrencyHelperFactory::make();
    }



    public function index()
    {
        $data = [

            'currencies'        => $this->currencyRepository->all(),
            'currentCurrencyId' => null,

        ];

        $currentCurrencyInstance = $this->currencyHelper->getCurrencyOrDefault();

        $hasCurrencyHelperHasError = $this->currencyHelper->errorThrower->hasError;

        if (!$hasCurrencyHelperHasError) {
            $data['currentCurrencyId'] = $currentCurrencyInstance->id;
        }
        $data['errors'] = $this->currencyHelper->errorThrower->getErrors();
        return $data;
    }

    public function create()
    {

        $this->currencyCrudForm->setFormParams(new StoreParams());
        $data = $this->currencyCrudFormQuickBuilder->build($this->currencyCrudForm);
        return $data;
    }

    public function updateOrInsert(Request $request,$id = null)
    {
        $this->currencyRepository->updateOrStore($request,$id);
    }

    public function edit($id)
    {
        $currency = $this->currencyRepository->getById($id);
        $this->currencyCrudForm->setCurrencyInstance($currency);
        $this->currencyCrudForm->setFormParams(new UpdateParams(['id'=>$id]));
        $data = $this->currencyCrudFormQuickBuilder->build($this->currencyCrudForm);
        return $data;
    }

    public function destroy($id)
    {
        $this->currencyRepository->delete($id);
    }


}
