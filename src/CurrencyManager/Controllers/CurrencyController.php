<?php

namespace CurrencyManager\Controllers;

use App\Http\Controllers\Controller;
use CurrencyManager\Factories\CurrencyCrudFormFactory;
use CurrencyManager\Factories\CurrencyHelperFactory;
use CurrencyManager\Factories\CurrencyRepositoryFactory;
use CurrencyManager\Forms\CurrencyCrudForm;
use CurrencyManager\Forms\StoreParams;
use CurrencyManager\Forms\UpdateParams;
use CurrencyManager\Repositories\CurrencyRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use CurrencyManager\Traits\CurrencyControllerAsTrait;

class CurrencyController extends Controller
{

    use CurrencyControllerAsTrait;

    public function __construct()
    {
        $this->__constructTrait();
    }

    public function index()
    {
        return $this->indexTraitMethod();
    }

    public function create()
    {
        return $this->createTraitMethod();
    }

    public function store(Request $request)
    {
        return $this->storeTraitMethod($request);
    }

    public function edit($id)
    {
        return $this->editTraitMethod($id);
    }

    /**
     * TODO: why did i put that "$id = $request->get("id");" ? there was a purpose? or i was drunk?
     */
    public function update(Request $request,$id)
    {
//        $id = $request->get("id");
        return $this->updateTraitMethod($request,$id);
    }

    public function destroy($id)
    {
        return $this->destroyTraitMethod($id);
    }

}
