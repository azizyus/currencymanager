<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 17.10.2018
 * Time: 03:56.
 */

namespace CurrencyManager\Controllers;

use App\Http\Controllers\Controller;
use CurrencyManager\Factories\CurrencyHelperFactory;
use CurrencyManager\Factories\CurrencyRepositoryFactory;
use Illuminate\Support\Facades\Redirect;

class CurrencyFrontController extends Controller
{
    public $currencyHelper;
    public $currencyRepository;

    public function __construct()
    {
        $this->currencyRepository = CurrencyRepositoryFactory::makeForFront();
        $this->currencyHelper = CurrencyHelperFactory::make();
    }

    public function change(Int $id)
    {
        $this->currencyHelper->setCurrencySession($id);

        return Redirect::back();
    }

    public function current()
    {
        $currency = $this->currencyHelper->getCurrencyOrDefault();
        if ($currency) {
            return [
                'id'         => $currency->id,
                'symbol'     => $currency->symbol,
                'multiplier' => $currency->multiplier,
            ];
        }

        return 'no default currency';
    }
}
