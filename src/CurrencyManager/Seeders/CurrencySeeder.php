<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 16.10.2018
 * Time: 04:52.
 */

namespace CurrencyManager\Seeders;

use CurrencyManager\Models\Currency;
use CurrencyManager\Models\CurrencyExtended;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::query()->truncate();
        $faker = Factory::create();
        for ($i = 0; $i < 25; $i++) {
            $currency = new CurrencyExtended();

            $currency->title = $faker->text(8);
            $currency->symbol = $faker->currencyCode;
            $currency->multiplier = $faker->randomFloat(2, 0, 9000);
            $currency->codeAlpha = $faker->text(5);
            $currency->codeNumeric = $faker->randomNumber(5);
            $currency->sort = $i;

            $currency->save();
            unset($currency);
        }

//        factory(CurrencyExtended::class,30)->create();
    }
}
