<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 17.10.2018
 * Time: 04:19.
 */

namespace CurrencyManager\Factories;

use App\Currency;
use CurrencyManager\Helpers\CurrencyHelper;

class CurrencyHelperFactory
{
    public static function make(Currency $defaultCurrency = null) : CurrencyHelper
    {
        $currencyHelper = new CurrencyHelper();
        if ($defaultCurrency != null) {
            $currencyHelper->setDefaultCurrency($defaultCurrency);
        }

        return $currencyHelper;
    }
}
