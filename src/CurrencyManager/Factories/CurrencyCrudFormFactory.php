<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 16.10.2018
 * Time: 05:40.
 */

namespace CurrencyManager\Factories;

use CurrencyManager\Forms\CurrencyCrudForm;

class CurrencyCrudFormFactory
{
    public function build(CurrencyCrudForm $crudForm)
    {
        $formAsString = $crudForm->formAsString();
        $formUrl = $crudForm->formParams->url();
        $formMethod = $crudForm->formParams->method();

        $build = [

            'formAsString'=> $formAsString,
            'formUrl'     => $formUrl,
            'formMethod'  => $formMethod,

        ];

        return $build;
    }
}
