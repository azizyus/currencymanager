<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 27.10.2018
 * Time: 00:53.
 */

namespace CurrencyManager\Factories;

use CurrencyManager\BaseQueryImplementations\CurrencyBaseQuery;
use CurrencyManager\BaseQueryImplementations\CurrencyBaseQueryOnlyActives;
use CurrencyManager\Repositories\CurrencyRepository;

class CurrencyRepositoryFactory
{
    public static function makeForCrud() : CurrencyRepository
    {
        $currencyRepository = new CurrencyRepository();
        $currencyRepository->setBaseQueryImplementation(new CurrencyBaseQuery());

        return $currencyRepository;
    }

    public static function makeForFront() : CurrencyRepository
    {
        $currencyRepository = new CurrencyRepository();
        $currencyRepository->setBaseQueryImplementation(new CurrencyBaseQueryOnlyActives());

        return $currencyRepository;
    }
}
