<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 26.10.2018
 * Time: 23:52.
 */

namespace CurrencyManager\BaseQueryImplementations;

use CurrencyManager\Models\CurrencyExtended;
use Illuminate\Database\Eloquent\Builder;

class CurrencyBaseQueryOnlyActives implements IBaseQuery
{
    public function makeBaseQuery(): Builder
    {
        return CurrencyExtended::where('isActive', true);
    }
}
