<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 26.10.2018
 * Time: 23:51.
 */

namespace CurrencyManager\BaseQueryImplementations;

use Illuminate\Database\Eloquent\Builder;

interface IBaseQuery
{
    public function makeBaseQuery() : Builder;
}
