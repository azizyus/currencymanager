<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 27.10.2018
 * Time: 01:46.
 */

namespace CurrencyManager\Tests;

use CurrencyManager\CurrencyManagerServiceProvider;

//use Orchestra\Testbench\TestCase as BaseTestCase;
use Orchestra\Testbench\BrowserKit\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->artisan('migrate', ['--database' => 'testing']);
    }

    protected function getPackageProviders($app)
    {
        return [
            CurrencyManagerServiceProvider::class,

        ];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'mysql');
    }

}
