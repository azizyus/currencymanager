<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 26.10.2018
 * Time: 23:02.
 */

namespace CurrencyManager\Tests\Unit;

use CurrencyManager\Helpers\CurrencySeederHelper;
use CurrencyManager\Models\Currency;
use CurrencyManager\Models\CurrencyExtended;
use CurrencyManager\Tests\TestCase;

class CurrencyRepositoryIsActiveNullTest extends TestCase
{
    /**
     * @test
     */
    public function test()
    {
        CurrencySeederHelper::fillTable();

        $currency = new Currency();
        $currency->multiplier = 3.14;
        $currency->save();

        $lastCurrency = CurrencyExtended::orderBy('id', 'DESC')->first();

        // not using because  1 !== true
        if ($lastCurrency->isActive == true) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
