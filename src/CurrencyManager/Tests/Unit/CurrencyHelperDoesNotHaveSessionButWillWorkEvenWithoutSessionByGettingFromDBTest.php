<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.10.2018
 * Time: 04:21.
 */

namespace CurrencyManager\Tests\Unit;

use CurrencyManager\Helpers\CurrencyHelper;
use CurrencyManager\Models\CurrencyExtended;
use CurrencyManager\Seeders\CurrencySeeder;
use CurrencyManager\Tests\TestCase;

class CurrencyHelperDoesNotHaveSessionButWillWorkEvenWithoutSessionByGettingFromDBTest extends TestCase
{
    /**
     * @test
     */
    public function test()
    {
        $this->flushSession();

        $currencySeeder = new CurrencySeeder();
        $currencySeeder->run();

        $currencyHelper = new CurrencyHelper();

        $currencyFromDB = CurrencyExtended::first();

        $currencyHelper->setDefaultCurrency($currencyFromDB);
        $foundCurrency = $currencyHelper->getDefaultCurrency();

        if ($foundCurrency->id === $currencyFromDB->id) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
