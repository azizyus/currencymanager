<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.10.2018
 * Time: 04:12.
 */

namespace CurrencyManager\Tests\Unit;

use CurrencyManager\Helpers\CurrencyHelper;
use CurrencyManager\Models\CurrencyExtended;
use CurrencyManager\Seeders\CurrencySeeder;
use CurrencyManager\Tests\TestCase;

class CurrencyHelperSetDefaultCurrencyDoesntOverrideSessionKeyTest extends TestCase
{
    /**
     * @test
     */
    public function test()
    {
        $currencySeeder = new CurrencySeeder();
        $currencySeeder->run();

        $currencyHelper = new CurrencyHelper();

        $mockedCurrency = new CurrencyExtended();
        $mockedCurrency->id = 9999;

        $this->flushSession();
        $this->session([$currencyHelper->sessionKey=>1]);

        $currencyHelper->setDefaultCurrency($mockedCurrency);
        $defaultSession = $currencyHelper->getCurrencyOrDefault();
        $currencyHelper->setDefaultCurrency($mockedCurrency);

        if ($defaultSession->id === 1) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
