<?php

namespace CurrencyManager\Unit;

use CurrencyManager\Helpers\CurrencyExchangeCalculator;
use CurrencyManager\Models\Currency;
use CurrencyManager\Tests\TestCase;

class CurrencyExchangeRateCalculationTest extends TestCase
{
    public $euroMultiplier = 5;
    public $turkishLiraMultiplier = 1;
    public $testPrice = 35;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $expectedResult = $this->testPrice * $this->euroMultiplier;
        $price = $this->testPrice; //considering its 35 TL and this will convert to €

        $turkishLira = new Currency();
        $turkishLira->multiplier = $this->turkishLiraMultiplier;

        $euro = new Currency();
        $euro->multiplier = $this->euroMultiplier;

        $currencyExchangeCalculator = new CurrencyExchangeCalculator($turkishLira, $euro);
        $currencyExchangeCalculator->setPrice($price);
        $exchangedPrice = $currencyExchangeCalculator->baseToTarget();

        if ($exchangedPrice == $expectedResult) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }

        echo "exchangedPrice was $exchangedPrice \n";
        echo "Your were expecting $expectedResult\n";
    }
}
