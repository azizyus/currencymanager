<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.10.2018
 * Time: 04:21.
 */

namespace CurrencyManager\Tests\Unit;

use CurrencyManager\Helpers\CurrencyHelper;
use CurrencyManager\Models\CurrencyExtended;
use CurrencyManager\Tests\BrowserTestCase;
use CurrencyManager\Tests\TestCase;

class CurrencyHelperDoesNotHaveSessionButWillSheUseMyDefaultCurrencyTest extends TestCase
{
    /**
     * @test
     */
    public function test()
    {

        $this->flushSession();

        $currencyHelper = new CurrencyHelper();

        $mockedCurrency = new CurrencyExtended();
        $mockedCurrency->id = 33;

        $currencyHelper->setDefaultCurrency($mockedCurrency);
        $foundCurrency = $currencyHelper->getDefaultCurrency();

        if ($foundCurrency->id === $mockedCurrency->id) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
