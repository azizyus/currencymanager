<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.10.2018
 * Time: 03:19.
 */

namespace CurrencyManager\Tests\Unit;

use CurrencyManager\Helpers\CurrencyHelper;
use CurrencyManager\Seeders\CurrencySeeder;
use CurrencyManager\Tests\TestCase;

class CurrencyHelperHasCorruptedSessionAndWillDefaultComesFromDBTest extends TestCase
{


    /**
     * @test
     */
    public function test()
    {
        $currencySeeder = new CurrencySeeder();
        $currencySeeder->run();

        $currencyHelper = new CurrencyHelper();

        $this->flushSession();
        $this->session([$currencyHelper->sessionKey=>'_SESSION_']); //giving 0 because db doesnt have 0 id

        $foundCurrency = $currencyHelper->getCurrencyOrDefault();

        if ($foundCurrency->id !== null) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
