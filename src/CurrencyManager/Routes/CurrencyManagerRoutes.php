<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.10.2018
 * Time: 00:32.
 */

namespace CurrencyManager\Routes;

use Illuminate\Support\Facades\Route;

class CurrencyManagerRoutes
{
    public static function get()
    {
        Route::group(['prefix'=>config('currencyManager.prefix'), 'middleware'=>config('currencyManager.middleWares'), 'namespace' => config('currencyManager.controllerNamespace')], function () {
            Route::resource('currency', 'CurrencyController');
        });

        Route::group(['prefix'=>config('currencyManager.frontPrefix'), 'middleware'=>config('currencyManager.frontMiddlewares'), 'namespace' => config('currencyManager.frontControllerNamespace')], function () {
            Route::get('change-currency-{id}', ['uses' => 'CurrencyFrontController@change', 'as' => 'currency.change']);
            Route::get('current-currency', ['uses' => 'CurrencyFrontController@current', 'as' => 'currency.current']);
        });
    }
}
