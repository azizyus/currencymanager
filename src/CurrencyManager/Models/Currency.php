<?php

namespace CurrencyManager\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $symbol
 * @property float $multiplier
 * @property string $codeAlpha
 * @property string $codeNumeric
 * @property int $sort
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property bool $isActive
 * @property bool $default
 */
class Currency extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['default','title', 'symbol', 'multiplier', 'codeAlpha', 'codeNumeric', 'sort', 'created_at', 'updated_at', 'deleted_at', 'isActive'];
}
