<?php

namespace CurrencyManager\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $title
 * @property string $symbol
 * @property float $multiplier
 * @property string $codeAlpha
 * @property string $codeNumeric
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class CurrencyExtended extends Currency
{
    use SoftDeletes;
    protected $table = 'currencies';
}
