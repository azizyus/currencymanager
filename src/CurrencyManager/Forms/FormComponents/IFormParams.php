<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 16.10.2018
 * Time: 05:21.
 */

namespace CurrencyManager\Forms;

interface IFormParams
{
    public function __construct(array $params = null);

    public function method() : String;

    public function url() : String;
}
