<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 16.10.2018
 * Time: 05:21.
 */

namespace CurrencyManager\Forms;

class StoreParams implements IFormParams
{
    public $params;

    public function __construct(array $params = null)
    {
        $this->params = $params;
    }

    public function url(): String
    {
        return route('currency.store');
    }

    public function method(): String
    {
        return FormEnums::$post;
    }
}
