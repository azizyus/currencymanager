<?php


namespace CurrencyManager\Forms;


use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class CurrencyForm extends Form
{

    public function buildForm()
    {
        $this->add('title',Field::TEXT);
        $this->add('symbol',Field::TEXT);
        $this->add('multiplier',Field::NUMBER);
        $this->add('codeAlpha',Field::TEXT);
        $this->add('codeNumeric',Field::TEXT);
        $this->add('sort',Field::NUMBER);
        $this->add('isActive',Field::CHOICE,[

            'choices' => [
                0 => 'No',
                1 => 'Yes'
            ]

        ]);
        $this->add('Complete',Field::BUTTON_SUBMIT);
    }

}
