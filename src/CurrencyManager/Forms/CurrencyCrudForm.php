<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 16.10.2018
 * Time: 03:40.
 */

namespace CurrencyManager\Forms;

use Former\Facades\Former;

/**
 * Class CurrencyCrudForm.
 */
class CurrencyCrudForm
{
    /**
     * @var null
     */
    public $currencyInstance;

    /**
     * @var IFormParams
     */
    public $formParams;

    /**
     * CurrencyCrudForm constructor.
     *
     * @param null $currencyInstance
     */
    public function __construct($currencyInstance = null)
    {
        if ($currencyInstance != null) {
            $this->currencyInstance = $currencyInstance;
        }
    }

    public function getFormMethod()
    {
        return $this->formParams->method();
    }

    public function getFormUrl()
    {
        return $this->formParams->url();
    }

    /**
     * @return mixed
     */
    public function getFormParams() : IFormParams
    {
        return $this->formParams;
    }

    /**
     * @param mixed $formParams
     */
    public function setFormParams(IFormParams $formParams): void
    {
        $this->formParams = $formParams;
    }

    /**
     * @return null
     */
    public function getCurrencyInstance()
    {
        return $this->currencyInstance;
    }

    /**
     * @param null $currencyInstance
     */
    public function setCurrencyInstance($currencyInstance): void
    {
        $this->currencyInstance = $currencyInstance;
    }

    /**
     * @return array
     */
    public function buildForm()
    {
        $inputsArray = [];
        Former::framework('TwitterBootstrap3');
        if ($this->hasModel()) {
            Former::populate($this->currencyInstance);
            $inputsArray[] = method_field('PUT');
        }

        $inputsArray[] = Former::group()->Class('form-group');
        $inputsArray[] = Former::text('title')->label('Title');
        $inputsArray[] = Former::closeGroup();

        $inputsArray[] = Former::group()->Class('form-group');
        $inputsArray[] = Former::text('symbol')->label('Symbol');
        $inputsArray[] = Former::closeGroup();

        $inputsArray[] = Former::group()->Class('form-group');
        $inputsArray[] = Former::number('multiplier')->label('Multiplier')->step('0.0001');
        $inputsArray[] = Former::closeGroup();

        $inputsArray[] = Former::group()->Class('form-group');
        $inputsArray[] = Former::text('codeAlpha')->label('Code Alpha');
        $inputsArray[] = Former::closeGroup();

        $inputsArray[] = Former::group()->Class('form-group');
        $inputsArray[] = Former::text('codeNumeric')->label('Code Numeric');
        $inputsArray[] = Former::closeGroup();

        $inputsArray[] = Former::group()->Class('form-group');
        $inputsArray[] = Former::inline_checkbox('isActive')->label('Is Active?');
        $inputsArray[] = Former::closeGroup();

        return $inputsArray;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function formAsString()
    {
        $form = $this->buildForm();

        $data = ['form'=>$form];

        return view('CurrencyManager::edit')->with($data);
    }

    /**
     * @return bool
     */
    public function hasModel() : Bool
    {
        if ($this->currencyInstance == null) {
            return false;
        }

        return true;
    }
}
