<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 16.10.2018
 * Time: 01:20.
 */

namespace CurrencyManager\Repositories;

use CurrencyManager\BaseQueryImplementations\CurrencyBaseQuery;
use CurrencyManager\BaseQueryImplementations\IBaseQuery;
use CurrencyManager\Models\CurrencyExtended;
use Illuminate\Http\Request;

class CurrencyRepository
{
    /**
     * @var IBaseQuery
     */
    protected $baseQueryImplementation;

    public function __construct()
    {
        $this->setBaseQueryImplementation(new CurrencyBaseQuery());
    }

    public function setBaseQueryImplementation(IBaseQuery $baseQuery)
    {
        $this->baseQueryImplementation = $baseQuery;
    }

    public function getById($id)
    {
        return $this->baseQuery()->find($id);
    }

    public function updateOrStore(Request $request, $id = null)
    {
        $request->merge(['id'=>$id]); //i cant remember but probably because of i wanted to use ->fill and request didnt have id so i injected it
        $rawRequest = $request->except(['_token', '_method']);
        $currency = $this->getById($id);

        if (!$currency) {
            $currency = new CurrencyExtended();
            $max = $this->baseQuery()->max('sort');
            if ($max == null) {
                $max = 0;
            }
            $currency->sort = ++$max;
        }

        $currency->fill($rawRequest);
        $currency->isActive = $request->input('isActive') != null ? true : false; //in crud page we have to detect exact value otherwise null will be cause exception

        $currency->save();
    }

    public function all()
    {
        return $this->baseQuery()->get();
    }

    public function baseQuery()
    {
        return $this->baseQueryImplementation->makeBaseQuery();
    }

    public function getFirst()
    {
        return $this->baseQuery()->first();
    }

    public function delete($id)
    {
        $currency = $this->getById($id);
        if ($currency) {
            $currency->delete();
        } //soft delete
    }

    public function deleteByInstance(CurrencyExtended $currencyExtended)
    {
        $id = $currencyExtended->id;
        $this->delete($id);
    }
}
