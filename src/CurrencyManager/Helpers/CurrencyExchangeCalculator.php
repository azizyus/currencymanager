<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 23.10.2018
 * Time: 23:06.
 */

namespace CurrencyManager\Helpers;

use CurrencyManager\Models\Currency;

class CurrencyExchangeCalculator
{
    public $baseCurrency;
    public $targetCurrency;
    public $baseMultiplier = 0;
    public $targetMultiplier = 0;
    public $price = 0;

    public function __construct(Currency $baseCurrency, Currency $targetCurrency)
    {
        $this->baseCurrency = $baseCurrency;
        $this->targetCurrency = $targetCurrency;
        $this->baseMultiplier = $baseCurrency->multiplier;
        $this->targetMultiplier = $targetCurrency->multiplier;
    }

    public function baseToTarget()
    {
        $price = $this->price;
        $baseMultiplier = $this->baseMultiplier;
        $targetMultiplier = $this->targetMultiplier;
        $calculatedPrice = ($price / $baseMultiplier) * $targetMultiplier;

        return $calculatedPrice;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPrice() : Int
    {
        return $this->price;
    }

    public function getBasePriceFormatted()
    {
        return number_format($this->price,2);
    }

    public function getBasePriceBeauty()
    {
        return $this->getBasePriceFormatted()." ".$this->baseCurrency->symbol;
    }

    public function getTargetPriceFormatted()
    {
        return number_format($this->baseToTarget(),2);
    }

    public function getTargetPriceBeauty()
    {
        return $this->getTargetPriceFormatted()." ".$this->targetCurrency->symbol;
    }

}
