<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 26.10.2018
 * Time: 23:10.
 */

namespace CurrencyManager\Helpers;

use CurrencyManager\Seeders\CurrencySeeder;

class CurrencySeederHelper
{
    public static function fillTable()
    {
        $currencySeeder = new CurrencySeeder();
        $currencySeeder->run();
    }
}
