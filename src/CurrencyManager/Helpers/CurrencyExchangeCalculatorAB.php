<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 23.10.2018
 * Time: 23:06.
 */

namespace CurrencyManager\Helpers;

use CurrencyManager\Models\Currency;

class CurrencyExchangeCalculatorAB extends CurrencyExchangeCalculator
{
  public function baseToTarget()
  {
      $price = $this->price;
      $targetMultiplier = $this->targetMultiplier;
      $calculatedPrice = $price / $targetMultiplier;

      return $calculatedPrice;
  }

}
