<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 17.10.2018
 * Time: 02:57.
 */

namespace CurrencyManager\Helpers;

use CurrencyManager\Error\ErrorThrower;
use CurrencyManager\Error\YouHaveNoCurrencyInYourDatabase;
use CurrencyManager\Factories\CurrencyRepositoryFactory;
use CurrencyManager\Models\CurrencyExtended;
use CurrencyManager\Repositories\CurrencyRepository;
use CurrencyManager\Tests\Unit\CurrencyHelperHasCorruptedSessionAndWillDefaultComesFromDBTest;

/**
 * Class CurrencyHelper.
 */
class CurrencyHelper
{
    /**
     * @var CurrencyExtended
     */
    public $currentCurrency;
    /**
     * @var string
     */
    public $sessionKey = 'user_currency';
    /**
     * @var null
     */
    public $sessionData = null;
    /**
     * @var CurrencyExtended
     */
    public $defaultCurrency;
    /**
     * @var CurrencyRepository
     */
    public $currencyRepository;

    public $errorThrower;

    /**
     * @return CurrencyRepository
     */
    public function getCurrencyRepository() : CurrencyRepository
    {
        return $this->currencyRepository;
    }

    /**
     * @param CurrencyRepository $currencyRepository
     */
    public function setCurrencyRepository(CurrencyRepository $currencyRepository): void
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * CurrencyHelper constructor.
     */
    public function __construct()
    {
        $currencyRepository = CurrencyRepositoryFactory::makeForFront();
        $this->setCurrencyRepository($currencyRepository);
        $this->errorThrower = new ErrorThrower();
    }

    /**
     * @return null
     */
    public function getDefaultCurrency()
    {
        return $this->defaultCurrency;
    }

    /**
     * @param null $defaultCurrency
     */
    public function setDefaultCurrency($defaultCurrency): void
    {
        $this->defaultCurrency = $defaultCurrency;
    }

    /**
     * @return CurrencyRepository|\Illuminate\Database\Eloquent\Model|null
     */
    public function getDefault()
    {
        $defaultCurrency = $this->defaultCurrency;
        if ($defaultCurrency == null) {
            $defaultCurrency = $this->currencyRepository->getFirst();
        }

        return $defaultCurrency;
    }

    /**
     * @param int $id
     *
     * @see  currency should be already exist otherwise it wont set
     */
    public function setCurrencySession(int $id)
    {
        $foundCurrency = $this->currencyRepository->getById($id);
        if ($foundCurrency) {
            session()->put($this->sessionKey, $foundCurrency->id);
        }
    }

    public function getSessionData()
    {
        return session()->get($this->sessionKey);
    }

    /**
     * @return CurrencyExtended
     */
    public function getCurrencyOrDefault()
    {
        $sessionData = $this->getSessionData();
        $this->sessionData = $sessionData;

        if ($this->currentCurrency === null) {
            //then check is there any already selected currencyId and probably not if you visiting first time this site
            if ($sessionData === null) {   //if its then put that got damn thing

                $defaultCurrency = $this->getDefault();
                if ($defaultCurrency) {
                    $this->sessionData = $defaultCurrency->id; //if there is no currency, basically you cant set it and the error in the below will warn you so its fine
                    $this->currentCurrency = $defaultCurrency;

                    //set default one because its empty ya know because of your empty session
                    $this->setCurrencySession($this->sessionData);
                }
            } else {
                $id = $sessionData;
                $currency = $this->currencyRepository->getById($id);
                if ($currency) {
                    $this->currentCurrency = $currency;
                } else {
                    /*
                     * @see CurrencyHelperHasCorruptedSessionAndWillDefaultComesFromDBTest
                     */
                    $this->currentCurrency = $this->getDefault();
                }
            }
        }

        if ($this->currentCurrency === null) {
            $this->errorThrower->setError(new YouHaveNoCurrencyInYourDatabase());
        }

        return $this->currentCurrency;
    }
}
