<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 16.10.2018
 * Time: 01:21.
 */
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrenciesTable extends Migration
{
    public $tableName = 'currencies';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('symbol')->nullable();
            $table->double('multiplier', 10, 2);
            $table->string('codeAlpha')->nullable();
            $table->string('codeNumeric')->nullable();
            $table->integer('sort')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
