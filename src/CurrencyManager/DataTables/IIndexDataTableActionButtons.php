<?php


namespace CurrencyManager\DataTables;


interface IIndexDataTableActionButtons
{

    public function build() : \Closure;

}
