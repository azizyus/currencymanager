<?php


namespace CurrencyManager\DataTables;
use CurrencyManager\Repositories\CurrencyRepository;
use Yajra\DataTables\Services\DataTable;


use App\Currencies\DataTables\IIndexDataTableActionButtons;

class CurrencyDataTable extends DataTable
{

    /**
     * @var string
     */
    protected $alias;
    /**
     * @var CurrencyRepository
     */
    protected $repository;

    /**
     * @var IIndexDataTableActionButtons
     */
    protected $dataTableActionButtons;

    public function __construct()
    {
        $this->repository = new CurrencyRepository();
        $this->alias = 'currency';
    }



    public function setIndexDataTableActionButtons(IIndexDataTableActionButtons $i)
    {
        $this->dataTableActionButtons = $i;
    }


    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'currencydatatable.action');
    }

    public function ajax()
    {

        return $this->dataTable($this->query())
            ->addColumn('action',$this->dataTableActionButtons->build())
            ->rawColumns(["action"])
            ->make(true);
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\Customers\Customer $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->repository->baseQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns([
                'id',
                'title',
                'symbol',
                'multiplier'
            ])
            ->minifiedAjax()
            ->addAction([
                "title"=>'Options',
                "width" => '100px'
            ])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Currencies_' . date('YmdHis');
    }

}


