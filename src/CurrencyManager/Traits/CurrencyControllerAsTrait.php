<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.11.2018
 * Time: 22:03
 */

namespace CurrencyManager\Traits;

use CurrencyManager\Controllers\ControllerComponent;
use CurrencyManager\Factories\CurrencyCrudFormFactory;
use CurrencyManager\Factories\CurrencyHelperFactory;
use CurrencyManager\Factories\CurrencyRepositoryFactory;
use CurrencyManager\Forms\CurrencyCrudForm;
use CurrencyManager\Forms\StoreParams;
use CurrencyManager\Forms\UpdateParams;
use CurrencyManager\Repositories\CurrencyRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

trait CurrencyControllerAsTrait
{



    private $editBladePath = '';
    private $indexBladePath = '';
    private $indexRedirectRoute = '';

    /**
     * @var ControllerComponent $controllerComponent
     */
    protected $controllerComponent;
    public function __constructTrait()
    {

        $this->indexRedirectRoute = $this->indexRedirectRoute();
        $this->editBladePath = $this->getEditBladePath();
        $this->indexBladePath = $this->getIndexBladePath();
        $this->controllerComponent = new ControllerComponent();
    }

    protected function getEditBladePath()
    {
        return 'CurrencyManager::editWrapper';
    }

    protected function getIndexBladePath()
    {
        return 'CurrencyManager::index';
    }

    protected function indexRedirectRoute()
    {
        return route('currency.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexTraitMethod()
    {

        $data = $this->controllerComponent->index();
        return view($this->indexBladePath)->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTraitMethod()
    {
        $data = $this->controllerComponent->create();
        return view($this->editBladePath)->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeTraitMethod(Request $request)
    {
        $this->controllerComponent->updateOrInsert($request);
        return Redirect::to($this->indexRedirectRoute);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function showTraitMethod($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function editTraitMethod($id)
    {
        $data = $this->controllerComponent->edit($id);
        return view($this->editBladePath)->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function updateTraitMethod(Request $request, $id)
    {
        $this->controllerComponent->updateOrInsert($request,$id);
        return Redirect::to($this->indexRedirectRoute);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyTraitMethod($id)
    {
        $this->controllerComponent->destroy($id);
        return Redirect::to($this->indexRedirectRoute);
    }

}
