
<h4>
    <a  href="{{route("currency.create")}}">Add</a>
</h4>

<ul>
    @foreach($errors as $error)

            <li>
                {{$error->errorMessage}}
            </li>

    @endforeach
</ul>

<ul>
    @foreach($currencies as $currency)


        <li @if($currency->isActive) style="color:green;" @else style="color:red;" @endif>
            @if($currentCurrencyId == $currency->id) + @endif
            <a href="{{route("currency.change",$currency->id)}}">{{$currency->title}}</a>
            <a href="{{route("currency.edit",$currency->id)}}">[EDIT]</a>
                <form action="{{route("currency.destroy",$currency->id)}}" method="POST">
                    {{method_field("DELETE")}}
                    {{csrf_field()}}

                    <input  type="submit" value="DELETE">
                </form>
        </li>

    @endforeach
</ul>
