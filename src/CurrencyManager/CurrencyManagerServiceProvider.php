<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 16.10.2018
 * Time: 01:37.
 */

namespace CurrencyManager;

use Illuminate\Support\ServiceProvider;

class CurrencyManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $migrationsPath = __DIR__.'/Migrations';
        $viewsPath = __DIR__.'/Views';
        $routeFile = __DIR__.'/Routes/routes.php';
        $configFile = __DIR__.'/Config/currencyManager.php';

        $this->mergeConfigFrom($configFile, 'currencyManager');
        $this->loadMigrationsFrom($migrationsPath);
        if (config('currencyManager.usePackageRoutes')) {
            $this->loadRoutesFrom($routeFile);
        }

        $this->loadViewsFrom($viewsPath, 'CurrencyManager');

        //config
        $this->publishes([

            $configFile=> config_path('currencyManager.php'),

        ], 'azizyus/currencymanager-publish-config');

        //migrations
        $this->publishes([

            __DIR__.'/Migrations/2017_01_17_151638_create_currencies_table.php'=> database_path('migrations/2017_01_17_151638_create_currencies_table.php'),

        ], 'azizyus/currencymanager-publish-migrations');

        //controllers
        $this->publishes([

            __DIR__.'/Controllers/CurrencyFrontController.php'=> app_path('Http/Controllers/CurrencyFrontController.php'),
            __DIR__.'/Controllers/CurrencyController.php'     => app_path('Http/Controllers/CurrencyController.php'),

        ], 'azizyus/currencymanager-publish-controllers');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
