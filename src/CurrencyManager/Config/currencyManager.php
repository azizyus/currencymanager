<?php


return [
    'usePackageRoutes'         => true,
    'middleWares'              => ['web'],
    'prefix'                   => 'currency',
    'frontPrefix'              => 'currency_front',
    'frontMiddlewares'         => ['web'],
    'controllerNamespace'      => 'CurrencyManager\Controllers',
    'frontControllerNamespace' => 'CurrencyManager\Controllers',
];
